// console.log("Hello")

const getCube = 2**3;
console.log(`The cube of 2 is: ${getCube}`)

const address = ["258 Washington Ave NW", "California", "90011"]
const [street, state, zip] = address;
console.log(`I live at ${street}, ${state} ${zip}`);

const animal = {
    Reptile: "Lolong",
    weight: `1075 kgs`,
    length: `20 ft 3 in`
};
const {Reptile, weight, length} = animal;
console.log(`${Reptile} was a saltwater crocodile. He weighed at ${weight} with a measurement of ${length}`);

const numbers = [1, 2, 3, 4, 5]
numbers.forEach((number) => {
    console.log(number);
})

const reduceNumber = numbers.reduce((total, num)=>{
    return total + num
})
console.log(reduceNumber)